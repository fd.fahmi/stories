from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response
from .views import *

class test_path(TestCase):
    def test_path(self):
        response = Client().get('/story9/login/',{},True)
        self.assertEqual(response.status_code,200)
    def test_path2(self):
        response = Client().get('/story9/signup/',{},True)
        self.assertEqual(response.status_code,200)
    def test_path3(self):
        response = Client().get('/story9/logout/',{},True)
        self.assertEqual(response.status_code,200)

class text_input(TestCase):
    def test_textHTMLLogin(self):
        response = Client().get('/story9/login/')
        html = response.content.decode('utf-8')
        self.assertIn("username", html)
        self.assertIn('password',html)
    def test_textHTMLDaftar(self):
        response = Client().get('/story9/signup/')
        html = response.content.decode('utf-8')
        self.assertIn("username",html)
        self.assertIn("password", html)
        self.assertIn("email", html)

class test_urls(TestCase):
    def setUp(self):
        self.login = reverse("story9:login")
        self.signup = reverse("story9:signup")
        self.logout = reverse("story9:logout")
    
    def test_login_use_right_function(self):
        found = resolve(self.login)
        self.assertEqual(found.func, loginUser)

    def test_signup_use_right_function(self):
        found = resolve(self.signup)
        self.assertEqual(found.func, createUser)

    def test_logout_use_right_function(self):
        found = resolve(self.logout)
        self.assertEqual(found.func, logoutUser)

class UserCreationFormTest(TestCase):
    def test_form(self):
        data = {
            'username': 'fahmifd',
            'email':'pahme@gmail.com',
            'password1': 'testz213',
            'password2': 'testz213',
        }
        form = UserCreationForm(data)
        self.assertTrue(form.is_valid())
