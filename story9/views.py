from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

def home(request):
    return render(request, 'success.html')

def createUser(request):
    form = CreateUserForm()

    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "✅ Akun berhasil dibuat, sekarang bisa login :).")
            return redirect('story9:login')

    context = {'form':form}
    return render(request, "signup.html", context)

def loginUser(request):
    
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username = username, password=password)

        if user is not None:
            login(request, user)
            return redirect('story9:home')
        else:
            messages.error(request,"Username atau password salah")

    return render(request, 'login.html')

def logoutUser(request):
    logout(request)
    return redirect('story9:login')