from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response

import story8.views

class SearchUnitTest(TestCase):
    def test_html_exist(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)
    
    def test_html_benar(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'search.html')

    def test_path(self):
        response = Client().get('/story8/',{},True)
        self.assertEqual(response.status_code,200)

    # def test_book(self):
    #     response = Client().get('/story8')
    #     html = response.content.decode('utf-8')
    #     self.assertIn("Search 1000+ Books Online",html)
    #     self.assertIn("Search books",html)

