from django.shortcuts import render
from django.http import JsonResponse
import requests
from django.core import serializers


def home(request):
    return render(request, "search.html")

def search(request):
    if (request.is_ajax()):
        book_api = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + request.GET['searchBar'])
        result = book_api.json()
        return JsonResponse(result)
