$(document).ready(function() {
    $(".panel").hide();
    $(".accordion").on('click', function() {
        $(this).next('.panel').slideToggle('slow');
    })
    $(function() {
        $('.up').on('click', function() {
          var wrapper = $(this).closest('.akord')
          wrapper.insertBefore(wrapper.prev())
        })
        $('.down').on('click', function() {
          var wrapper = $(this).closest('.akord')
          wrapper.insertAfter(wrapper.next())
        })
      })
});
